package moc;

/**
 * ExceptionSUT = System Under Test - zaczerpnięte z książki Tomka Kaczanowskiego
 * Ten tutaj jest dla wyjątków.
 */
public interface ExceptionSUT {

    void rzucaWyjątkiem(Exception e) throws Exception;

    void rzucaWyjątkiemPoProstu() throws Exception;
}
