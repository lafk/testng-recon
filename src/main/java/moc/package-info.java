/**
 * <ol>Kolejność:
 * <li>domyślny pakiet - jak to zestawić</li>
 * <li>pakiet start - pierwsze kroki z TestNG</li>
 * <li>pakiet <b>moc</b> - tu jesteś</li>
 * <li>wykonanie - jak uruchomić i na przebieg wpłynąć</li>
 * <li>dalej - ciekawostki, co dalej, zaawansowane</li>
 * </ol>
 * Mam tę moc! - czyli najpotężniejsze i najciekawsze narzędzia.
 * <ol>Kroki tutaj:
 * <li>WieleAsercjiNaMiękkichAsercjach - czyli wiele asercji przez wiele testów lub <b>jedną miękką asercję</b></li>
 * <li>AdnotacjaTest - możliwości adnotacji @Test</li>
 * <li>WyjątkowyTest - oczekiwane wyjątki</li>
 * <li>WyjątkowyTestWPułapce - oczekiwane wyjątki i problemy z nimi</li>
 * <li>GrupujTesty - omawia grupowanie testów</li>
 * <li>... - testy parametryczne na różne sposoby</li>
 * <li>SparametryzowanyTest - poznajemy dostawców danych</li>
 * <li>SparametryzowanyTestZKlasą - wpółdzielimy dostawców danych między testami</li>
 * </ol>
 *
 */
package moc;