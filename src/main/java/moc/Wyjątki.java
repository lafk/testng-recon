package moc;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Pokazówka wyjątków
 *
 * @author Tomasz @LAFK_pl Borek
 */
class Wyjątki {

    public static void main(String[] args) {
        try {
            rzućMójWłasnyWyjątek();
            rzućIOE();
            rzućFNF();
        } catch (Exception e) {
   //         throw new UserRegistratrionException(e.getMessage()), e.getCause())
            System.err.println(e.getMessage());
        }
    }

    private static void rzućMójWłasnyWyjątek() throws MójWłasnyWyjątekException {
        throw new MójWłasnyWyjątekException();
    }

    private static void rzućIOE() throws IOException {
        throw new IOException("problem wejścia wyjścia");
    }

    private static void rzućFNF() throws FileNotFoundException {
        throw new FileNotFoundException("nie ma pliku");
    }
}
