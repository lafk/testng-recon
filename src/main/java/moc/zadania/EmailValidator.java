package moc.zadania;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Thanks HowToDoInJava for a great page illustrating the problem
 *
 *
 * @see <a href="https://howtodoinjava.com/regex/java-regex-validate-email-address/">Page that inspired this</a>
 * @see <a href="https://www.rfc-editor.org/rfc/rfc5322.txt">Appropriate RFC</a>
 * @author Tomasz @LAFK_pl Borek
 */
class EmailValidator {

    private String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
    private List<String> emails;

    EmailValidator(String regex, List<String> emails) {
        this.regex = regex;
        this.emails = emails;
    }

    void setRegex(String regex) {
        this.regex = regex;
    }

    void setEmails(List<String> emails) {
        this.emails = emails;
    }

    boolean isValid(String email) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        final boolean itIs = matcher.matches();
        System.out.format("[INFO]: email %s matches: %s", email, itIs);
        return itIs;
    }

    boolean areValid() {
        Pattern pattern = Pattern.compile(regex);
        for(String email : emails){
            Matcher matcher = pattern.matcher(email);
            final boolean itIs = matcher.matches();
            System.out.format("[INFO]: email %s matches: %s", email, itIs);
            if (!itIs) return false;
        }
        return true;
    }
}
