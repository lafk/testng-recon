package moc.zadania;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Thanks HowToDoInJava for a great page illustrating the problem
 *
 *
 * @see <a href="https://howtodoinjava.com/regex/java-regex-validate-email-address/">Page that inspired this</a>
 * @see <a href="https://www.rfc-editor.org/rfc/rfc5322.txt">Appropriate RFC</a>
 * @author Tomasz @LAFK_pl Borek
 */
class BadEmailValidator {


    static boolean isValid(String email) {
        String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    static boolean areValid(List<String> emails) {
        String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
        Pattern pattern = Pattern.compile(regex);

        for(String email : emails){
            Matcher matcher = pattern.matcher(email);
            final boolean itIs = matcher.matches();
            System.out.format("[INFO]: email %s matches: %s", email, itIs);
            if (!itIs) {
                return itIs;
            }
        }
        return true;
    }
}
