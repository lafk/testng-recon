package moc.zadania;

/**
 * Thank you Tomek K. for a good example
 *
 * @author Tomasz Kaczanowski
 */
class Money {

    private final int amount;
    private final String currency;

    Money(int amount, String currency) {
        if (amount < 0) {
            throw new IllegalArgumentException("negative amount is illegal");
        }
        if (currency == null || currency.isEmpty()) {
            throw new IllegalArgumentException("currency can't be null or empty");
        }
        this.amount = amount;
        this.currency = currency;
    }

    @Override
    public String toString() {
        return String.format("Money: %d of %s", amount, currency);
    }
}
