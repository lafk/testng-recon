package moc.zadania;

/**
 * Java Calculator on int types, double-operand
 *
 * @author Tomasz @LAFK_pl Borek
 */
class Calculator {

    int add(int x, int y) {
        return x+y;
    }
    int subtract(int x, int y) {
        return x-y;
    }
    int times(int x, int y) {
        return x*y;
    }
    int divide(int x, int y) {
        return x/y;
    }
    int modulo(int x, int y) {
        return x%y;
    }
}
