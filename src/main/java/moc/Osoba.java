package moc;

/**
 * Nieśmiertelna klasa Osoba, przerabiana na wiele sposobów w wielu programach. Tutaj jest prosta - ma po prostu rzeczy potrzebne do wykonania na niej wielu asercji.
 *
 * @author Tomasz @LAFK_pl Borek
 */
class Osoba {

    private int wiek;
    private String zawód;
    private String imię;
    private String nazwisko;

    Osoba() {}

    Osoba(String zawód, String imię, String nazwisko, int wiek) {
        this.zawód = zawód;
        this.wiek = wiek;
        this.imię = imię;
        this.nazwisko = nazwisko;
    }

    int getWiek() {
        return wiek;
    }

    String getZawód() {
        return zawód;
    }

    String getImię() {
        return imię;
    }

    String getNazwisko() {
        return nazwisko;
    }

    @Override
    public String toString() {
        return "Osoba{" +
                "wiek=" + wiek +
                ", zawód='" + zawód + '\'' +
                ", imię='" + imię + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                '}';
    }

}
