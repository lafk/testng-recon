/**
 * <ol>Kolejność:
 * <li>domyślny pakiet - jak to zestawić</li>
 * <li>pakiet <b>start</b> - tu jesteś</li>
 * <li>wykonanie - jak uruchomić i na przebieg wpłynąć</li>
 * <li>dalej - ciekawostki, co dalej, zaawansowane</li>
 * </ol>
 * Podstawy podstaw - czyli proste testy i asercje, początki adnotacji @Test, nic skomplikowanego.
 * <ol>Kroki tutaj:
 * <li>KlasowyApkiTest - omawia @Test na klasie</li>
 * <li>KlasowyLimitowanyApkiTestPYT - PYTANIE?</li>
 * <li>KlasowyLimitowanyApkiTestODP - ODPowiedź! limity @Test na klasie</li>
 * <li>MetodycznyLimitowanyApkiTest - to samo z @Test na metodach - są różnice?</li>
 * <li>MetodycznyApkiTest - pokazuje ten sam test ale to metody są zaadnotowane, nie klasa</li>
 * <li>TestRezultatów - czas poznać, jakie możliwe rezultaty są gdy wykonujemy test!</li>
 * <li>InspekcjeIDETest - czyli złe asercje</li>
 * </ol>
 *
 * @see <a href="http://kaczanowscy.pl/tomek/2012-04/why-testng-and-not-junit">Czemu TestNG a nie JUnit</a>
 * @see <a href="http://testng.org/doc/documentation-main.html">Oficjalna dokumentacja TestNG</a>
 * @see <a href="http://testng.org/doc/idea.html">Współpraca z Intellij IDEA</a>
 * @see <a href="http://testng.org/doc/eclipse.html">Współpraca z Eclipse</a>
 * @see plik `gradle.adoc` w katalogu głównym projektu
 */
package start;