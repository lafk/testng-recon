package wykonanie;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Spójrz w XML <code>2.xml</code> w zasobach testowych.
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Test
public class DrugaKlasaTestowa {

    private Object jakoPole = new Object();

    public void porównajmyDwaObiektyTożsamościowo() {
        Assert.assertSame(new Object(), new Object());
    }

    public void porównajmyIdentycznośćPolaIZmiennejLokalnej() {
        Object jakoZmiennaLokalna = new Object();
        Assert.assertSame(jakoPole, jakoZmiennaLokalna);
    }
}
