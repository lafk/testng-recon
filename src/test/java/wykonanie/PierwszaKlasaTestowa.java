package wykonanie;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Spójrz w XML <code>1.xml</code> w zasobach testowych.
 *
 * @author Tomasz @LAFK_pl Borek
 */
public class PierwszaKlasaTestowa {

    @Test
    public void porównaniePrymitywówIntówDziała() {
        Assert.assertEquals(1, 1, "1 != 1");
    }

    @Test
    public void porównaniePrymitywówCharówDziała() {
        Assert.assertEquals('1', '1', "'1' != '1'");
    }
}
