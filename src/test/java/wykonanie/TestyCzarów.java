package wykonanie;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


/**
 * Demonstracja grup z IDE i nie tylko.
 *
 * Przeczytaj kod - odpal testy - popatrz na TODO
 *
 * TODO: Ministerstwo Magii wymaga, by przed każdym demonstracyjnym użyciem zaklęć zabronionych (niewybaczalnych) okazać zebranym pozwolenie na takową demonstrację. Jak to zrobić?
 *
 * @author Tomasz "@LAFK_pl" Borek
 * @see <a href="https://stackoverflow.com/questions/3602404/intellij-idea-testng-run-a-method-before-each-test-in-a-group">Intellij ma problem z grupami jeśli ich "nie zna"</a>
 */
public class TestyCzarów {

    private String zaklęcie = "czary mary";

    @Test(groups = { "proste", "tom I" })
    public void naDrzwi() {
        zaklęcie = "alohom-o-ra";
        System.out.println(zaklęcie);
    }

    @Test(groups = { "tom I", "z dwu słów" })
    public void naTrolla() {
        zaklęcie = "vingardium levi-o-sa";
        System.out.println(zaklęcie);
    }

    @Test(groups = { "tom III" })
    public void naMokreOkulary() {
        zaklęcie = "impervius";
        System.out.println(zaklęcie);
    }

    @Test(groups = { "ZABRONIONE", "tom IV", "tom V", "tom VI" })
    public void dlaPrzymusu() {
        zaklęcie = "imperio";
        System.out.println(zaklęcie);
    }

    @Test(groups = { "ZABRONIONE", "tom IV", "tom VI", "tom VII" })
    public void dlaTortur() {
        zaklęcie = "crucio";
        System.out.println(zaklęcie);
    }

    @Test(groups = { "ZABRONIONE", "z dwu słów", "tom IV", "tom VI", "tom VII" })
    public void dlaMordu() {
        zaklęcie = "avada kedavra";
        System.out.println(zaklęcie);
    }

}