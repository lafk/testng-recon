package wykonanie;

import org.testng.annotations.*;

/**
 * Klasa pokazuje kroki wykonania testów i co zrobić, by w którymś z nich coś się stało.
 * <li>Suita = zestaw testów</li>
 * <li>Test = znacznik <code><test>w pliku XML</test></code></li>
 * <li>Klasa Testowa = plik Java w którym są testy, tu można spotkać klasową adnotację @Test</li>
 * <li>Metoda Testowa = przypadek testowy = metoda, w której są testy, tu @Test na metodzie</li>
 *
 * @author Tomasz @LAFK_pl Borek
 */
public class TestGdzieMożnaSięWpiąćWTesty {

    @Test
    public void testCase1() {
        System.out.println("test 1");
    }

    @Test
    public void testCase2() {
        System.out.println("test 2");
    }

    @BeforeMethod
    public void beforeMethod() {
        System.out.println("Before Method");
    }

    @AfterMethod
    public void afterMethod() {
        System.out.println("After Method");
    }

    @BeforeClass
    public void beforeClass() {
        System.out.println("Before Class");
    }

    @AfterClass
    public void afterClass() {
        System.out.println("After Class");
    }

    @BeforeTest
    public void beforeTest() {
        System.out.println("Before Test");
    }

    @AfterTest
    public void afterTest() {
        System.out.println("After Test");
    }

    @BeforeGroups
    public void beforeGroups() {
        System.out.println("Before Groups - notice me sempai");
    }

    @AfterGroups
    public void afterGroups() {
        System.out.println("After Groups - notice me sempai");
    }

    @BeforeSuite
    public void beforeSuite() {
        System.out.println("Before Suite");
    }

    @AfterSuite
    public void afterSuite() {
        System.out.println("After Suite");
    }

}