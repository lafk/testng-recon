package wykonanie;

import org.testng.annotations.Test;

/**
 * @author Tomasz @LAFK_pl Borek
 */
@Test
public class TrzeciaKlasaTestowa {

    public void prostaAsercja() {
        assert true : "Prawda to nieprawda?!";
    }

    public void prostaAsercjaFałszJestFałszem() {
        assert !false : "Fałsz to nie fałsz?!";
    }
}
