package moc;

import org.testng.annotations.DataProvider;

/**
 * @author Tomasz @LAFK_pl Borek
 */
public class DP {

    // Dostarcza dane każdej metodzie testowej czerpiącej z dostawcy "imięOrazWiek"
    @DataProvider(name = "imięOrazWiek")
    public Object[][] nieistotneJakJąNazwę() {
        return new Object[][]{
                {"Cedric", 36},
                {"Anne", 37},
                {"Tommy", 8}
        };
    }

}
