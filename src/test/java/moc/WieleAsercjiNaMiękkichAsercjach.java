package moc;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static org.testng.Assert.*;

/**
 * Problem: w jednym poprzednim teście ({@link start.InspekcjeIDETest}) mieliśmy dużo asercji a wykonało się ich... niedużo!
 *
 * <ol><b>Jak właściwie podejść do problemu wielu asercji?</b>
 *     <li>można rozbić na wiele testów, zasada: 1 asercja na 1 test</li>
 *     <li>można użyć miękkich asercji</li>
 * </ol>
 * @apiNote Miękkie asercje są w klasie {@link org.testng.asserts.SoftAssert}. Tworzymy instancję tejże, używamy na tej instancji asercji 'zwykłych' a na koniec zbieramy i wykonujemy wszystko przez `assertAll()`.
 * @author Tomasz @LAFK_pl Borek
 */
@Test
public class WieleAsercjiNaMiękkichAsercjach {

    private Osoba pusta = new Osoba();

    public void wieleAsercjiNaTwardo() {
        assertNotNull(pusta.getImię(), "imię nullem");
        assertNotNull(pusta.getNazwisko(), "nazwisko nullem");
        assertNotNull(pusta.getZawód());
        assertNotNull(pusta.toString());
    }

    public void miękkieAsercje() {
        SoftAssert sa = new SoftAssert();
        sa.assertNotNull(pusta.getImię(), "imię nullem");
        sa.assertNotNull(pusta.getNazwisko(), "nazwisko nullem");
        sa.assertNotNull(pusta.getZawód(), "zawód nullem");
        sa.assertNotNull(pusta.toString(), "toString nullem");
        sa.assertAll();
    }

    //TODO: jak powyższe przeczytane i w miarę pojęte, usuńmy linię poniżej (@Test)
    public void pisarzStanisławLem81JestDobrzeWypełniony() {
        Osoba pisarzStanisławLem81 = new Osoba("pisarz", "Stanisław", "Lem", 81);
        //TODO: użyj miękkich asercji, porównuj wartości a nie tylko czy null, nie pomiń wieku!
        SoftAssert sofcik = new SoftAssert();
        sofcik.assertEquals(pisarzStanisławLem81.getImię(), "Stanisław", "Stanisław Lem nie ma na imię Stanisław?!");
        sofcik.assertEquals(pisarzStanisławLem81.getNazwisko(), "Lem", "Stanisław LEM nie powinien mieć innego nazwiska niż LEM");
        sofcik.assertEquals(pisarzStanisławLem81.getZawód(), "pisarz", "Stanisław Lem to pisarz z zawodu, zaakceptuję też geniusz");
        sofcik.assertEquals(pisarzStanisławLem81.getWiek(), 81, "Stanisław Lem - nestor, lat 81... tutaj jednak NIE. Czemu?");
        sofcik.assertAll();
    }
}