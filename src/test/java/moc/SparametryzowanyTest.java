package moc;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 * Napoteżniejszą cechą TestNG są dostawcy danych - prosta i przyjemna parametryzacja testów.
 * <ol>W krokach idzie to tak:
 * <li>Adnotujemy metodę testową <code>@Test(dataProvider="nazwaMetodyDostawczej")</code>.</li>
 * <li>Adnotujemy metodę dostawczą <code>@DataProvider</code>.</li>
 * <li>Metoda testowa powinna mieć odpowiednie parametry.</li>
 * <li>Metoda dostawcza niech zwraca matrycę obiektów.</li>
 *
 * @author LAFK_pl, Tomasz.Borek@gmail.com
 */
public class SparametryzowanyTest {

    // Dostarcza dane każdej metodzie testowej czerpiącej z dostawcy "imięOrazWiek"
    @DataProvider(name = "imięOrazWiek")
    public Object[][] nieistotneJakJąNazwę() {
        return new Object[][]{
                {"Cedric", 36},
                {"Anne", 37},
                {"Tommy", 8}
        };
    }

    @Test(dataProvider = "imięOrazWiek")
    public void wydrukujZestawyTestowe(String imię, Integer wiek) {
        System.out.println(imię + " " + wiek);
    }

    @Test(dataProvider = "imięOrazWiek")
    public void zweryfikujObecnośćDanych(String imię, Integer wiek) {
        Assert.assertNotNull(imię, "Name null for " + imię + wiek);
        Assert.assertNotNull(wiek, "Age null for " + imię + wiek);
    }

    @Test(dataProvider = "imięOrazWiek")
    public void weryfikacjaDorosłości(String imię, Integer wiek) {
        Assert.assertTrue(wiek > 18, imię + " niepełnoletni w EU!");
        Assert.assertTrue(wiek > 21, imię + " niepełnoletni w USA!");
    }
}