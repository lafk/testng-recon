package moc;

import org.testng.annotations.Test;

/**
 * Czy to działa! Nie przez zależności, ale przez GRUPY.
 *
 * @author Tomasz @LAFK_pl Borek
 */
public class TestOsobnychZależnościJakiśTest {

    @Test(groups = { "jakiś-test" })
    public void groupTestTwo() {
        System.out.println("Group test method two");
    }

    @Test(groups = { "jakiś-test" })
    public void groupTestThree() {
        System.out.println("Group Test method three");
    }
}
