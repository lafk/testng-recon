package moc;

import org.testng.annotations.Test;

/**
 * A co jeśli zależnych metod jest wiele?
 *
 * @author Tomasz @LAFK_pl Borek
 */
public class TestWieluZależności {

    @Test(dependsOnMethods = { "testTwo", "testThree" })
    public void testOne() {
        System.out.println("Test method one");
    }

    @Test
    public void testTwo() {
        System.out.println("Test method two");
    }

    @Test
    public void testThree() {
        System.out.println("Test method three");
    }
}
