package moc;

import org.testng.annotations.Test;

/**
 * No więc... nie
 *
 * @author Tomasz @LAFK_pl Borek
 */
public class TestZależnościODP {
    
    @Test(dependsOnMethods = { "theTestTwo" })
    public void aTestOne() {
        System.out.println("Test method one");
    }

    @Test
    public void theTestTwo() {
        System.out.println("Test method two");
    }

}
