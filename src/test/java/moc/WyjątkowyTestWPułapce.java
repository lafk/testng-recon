package moc;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Niezależnie, czy robimy to w starym czy nowym stylu, jest problem z testowaniem wyjątków.
 * A nawet więcej niż jeden.
 * TODO: Krótka dyskusja...
 *
 * Zademonstrujmy!
 *
 * @author Tomasz @LAFK_pl Borek
 */
public class WyjątkowyTestWPułapce {

    @Test
    public void wStarymStylu() {
        // given
        String padłoPonieważ = "nie poleciał wyjątek";
        ExceptionSUT wyjątkowySUT = new RzucającaWyjątkami();
        Exception oczekiwany = new Exception();

        // when
        try {
            wyjątkowySUT.rzucaWyjątkiem(oczekiwany);
        } catch (Exception e) {
            // then
            Assert.assertEquals(e.getClass(), oczekiwany.getClass(), "klasa wyjątku jest inna niż oczekiwano!");
            return;
        }
        Assert.fail(padłoPonieważ);
    }

    @Test(expectedExceptions = Exception.class)
    public void wNowymStylu() throws Exception {
        // given
        ExceptionSUT exceptionSut = new RzucającaWyjątkami();

        // when
        exceptionSut.rzucaWyjątkiemPoProstu();
    }
}
//TODO: GWT czy AAA i o co w tym chodzi?
