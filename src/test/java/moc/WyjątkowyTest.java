package moc;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Czyli czas na obsługę nieszczęśliwej ścieżki - ona sama pewnie preferuje nazywać się wyjątkową!
 *
 * @author Tomasz @LAFK_pl Borek
 */
public class WyjątkowyTest {

    @Test
    public void wStarymStylu() {
        // given
        String padłoPonieważ = "nie poleciał wyjątek";
        ExceptionSUT rzucającyWyjątkamiSUT = new RzucającaWyjątkami();
        Exception oczekiwany = new Exception();

        // when
        try {
            rzucającyWyjątkamiSUT.rzucaWyjątkiem(oczekiwany);
        } catch (Exception e) {
            // then
            Assert.assertEquals(e.getClass(), oczekiwany.getClass(), "klasa wyjątku jest inna niż oczekiwano!");
            return;
        }
        Assert.fail(padłoPonieważ);
    }

    //TODO: przeróbmy powyższe na coś ciekawszego używając adnotacji @Test

    @Test(expectedExceptions = {Exception.class, AssertionError.class})
    public void wyjątekPoleciećPowinien() throws Exception {
        // given
        ExceptionSUT rzucającyWyjątkamiSUT = new RzucającaWyjątkami();
        // when
        rzucającyWyjątkamiSUT.rzucaWyjątkiem(new Exception());
    }
}
