package moc;

import org.testng.annotations.Test;

/**
 * TODO: rozbij na klasy. Wywołaj i sprawdź czy to działa!
 *
 * @author Tomasz @LAFK_pl Borek
 */
public class TestDziedziczonychZależności {

    public class RodzicTest
    {
        @Test(dependsOnMethods = { "testTwo" })
        public void testOne() {
            System.out.println("Test method one");
        }

        @Test
        public void testTwo() {
            System.out.println("Test method two");
        }
    }

    public class DzieckoTest extends RodzicTest
    {
        @Test(dependsOnMethods = { "testOne" })
        public void testThree() {
            System.out.println("Test three method in Inherited test");
        }

        @Test
        public void testFour() {
            System.out.println("Test four method in Inherited test");
        }
    }}
