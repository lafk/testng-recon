package moc.files_task;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.testng.Assert.assertTrue;

/**
 * Creates fileNames to later create files
 *
 * @author Tomasz @LAFK_pl Borek
 */
public class FilesCreatorOldTest {


    @Test(groups = {"positive"})
    public void createFileWithPermanentName() {
        // Test
        String fileName = "new_file1.txt";
        FileHelper fileHelper = new FileHelper();
        fileHelper.createFile(fileName);

        // Asserts
        assertTrue(fileHelper.isFilePresent(fileName), "[ERROR] No file '" + fileName + "' in folder.");
    }


    @Test(groups = {"positive"})
    public void createFileWithRandomName()  {
        // Test
        FileHelper fileHelper = new FileHelper();
        String fileName = "new_file"+ (fileHelper.getRandomNumber(1,100)).toString()+".txt";
        fileHelper.createFile(fileName);

        // Asserts
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(fileName.length() > 0, "[ERROR] File`s length is 0.");
        softAssert.assertEquals(fileName.substring(0,8), "new_file", "[ERROR] File name is incorrect.");
        softAssert.assertAll();

    }


    @Test(groups = {"negative"})
    public void createFileWithEmptyName()  {
        // Test
        FileHelper fileHelper = new FileHelper();
        fileHelper.createFile("");

        // Asserts
        assert !(new File("")).exists();
    }


    @Test(groups = {"negative"})
    public void createFileWithAlreadyExistingName()  {
        // Test
        String fileName = "new_file1.txt";
        FileHelper fileHelper = new FileHelper();

        // Create file if it doesn`t exist
        if (!fileHelper.isFilePresent(fileName)){
            fileHelper.createFile(fileName);
        }

        fileHelper.createFile(fileName);

        // Asserts
        assertTrue(fileHelper.isTheOnlyFileWithThisName(), "There is more than 1 file named: " + fileName);
    }

}