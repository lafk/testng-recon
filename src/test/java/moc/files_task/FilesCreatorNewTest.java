package moc.files_task;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.testng.Assert.assertTrue;

/**
 * Creates files the new way
 *
 * @author Tomasz @LAFK_pl Borek
 */
public class FilesCreatorNewTest {

    /* Passing parameters with .xml file */
    @Test(groups = {"positive"}, testName = "File Creation Test")
    @Parameters({"fileName", "fileFormat"})
    public void createFileWithCombinedName(String fileName, String fileFormat) {
        // Test
        String combinedName = fileName + "." + fileFormat;
        FileHelper fileHelper = new FileHelper();
        fileHelper.createFile(combinedName);

        // Asserts
        assertTrue(fileHelper.isFilePresent(combinedName), "[ERROR] No file '" + combinedName + "' in folder.");
    }


    /* Passing parameters with Data Provider that generates random names */
    @Test(groups = {"positive"}, dataProvider = "generateRandomFileNames")
    public void createFileWithNameFromDataProvider(String name, String format) {
        // Test
        String combinedName = name + format;
        FileHelper fileHelper = new FileHelper();
        fileHelper.createFile(combinedName);

        // Asserts
        assertTrue(fileHelper.isFilePresent(combinedName), "[ERROR] No file '" + combinedName + "' in folder.");
    }


    /* Passing parameters with Data Provider that loads names from file */
    @Test(groups = {"positive"}, dataProviderClass = DataProviders.class, dataProvider = "loadNamesFromFile")
    public void createFileWithNameLoadedFromFile(String name, String format) {
        // Test
        String combinedName = name + format;
        FileHelper fileHelper = new FileHelper();
        fileHelper.createFile(combinedName);

        // Asserts
        assertTrue(fileHelper.isFilePresent(combinedName), "[ERROR] No file '" + combinedName + "' in folder.");
    }


    @DataProvider
    public Iterator<Object[]> generateRandomFileNames() {
        List<Object[]> data = new ArrayList<Object[]>();
        FileHelper fileHelper = new FileHelper();

        for (int i = 0; i < 10; i++) {
            data.add(new Object[]{
                    fileHelper.generateRandomFileName(),
                    fileHelper.generateRandomFileFormat()
            });
        }
        return data.iterator();
    }
}
