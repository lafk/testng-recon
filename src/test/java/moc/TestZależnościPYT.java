package moc;

import org.testng.annotations.Test;

/**
 * TODO: Czy to się wywołuje alfabetycznie?
 *
 * @author Tomasz @LAFK_pl Borek
 */
public class TestZależnościPYT {
    
    @Test(dependsOnMethods = { "testTwo" })
    public void testOne() {
        System.out.println("Test method one");
    }

    @Test
    public void testTwo() {
        System.out.println("Test method two");
    }

}
