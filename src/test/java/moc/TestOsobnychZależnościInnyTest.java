package moc;

import org.testng.annotations.Test;

/**
 * Czy to działa! Nie przez zależności, ale przez GRUPY.
 *
 * @author Tomasz @LAFK_pl Borek
 */
public class TestOsobnychZależnościInnyTest {

    @Test(dependsOnGroups = { "jakiś-test" })
    public void groupTestOne() {
        System.out.println("Group Test method one");
    }

}
