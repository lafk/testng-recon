import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Podstawowe testy <b>Nowej Generacji</b>.
 *
 * @apiNote Metody klasy Assert często są przeciążone by przyjmować wiadomości.
 * @author Tomasz "@LAFK_pl" Borek
 */
public class ApkiTest {

    @Test
    public void testA() {
        Assert.assertFalse(true);
    }

    @Test
    public void testB() {
        final boolean condition = true;
        Assert.assertFalse(condition, "oczekiwałem fałszu a miałem " + condition);
    }
}