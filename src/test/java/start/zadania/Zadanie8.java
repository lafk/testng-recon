package start.zadania;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

/**
 * Napisz po jednym teście na możliwe rezultaty testów.
 *
 * Podpowiedź: jest ich 4.
 *
 * Test nie brany pod uwagę przez TestNG może być jako piąty, zaliczę go.
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Test
public class Zadanie8 {

    public void sukces() {
        assertEquals(1, 1);
    }

    public void porażkaNaAsercji() {
        assertEquals(2, 1);
    }

    public void poleciałWyjątek() {
        throw new RuntimeException("wyjątek rzucony z testu");
    }

    public void poleciałWyjątekPomijającyTest() {
        throw new SkipException("wyjątek pomijający test");
    }

    @Test(enabled = false)
    public void testNieWziętyPodUwagęPrzyTestowaniu() {
        assertEquals(1, 1, "pominięty");
    }



}
