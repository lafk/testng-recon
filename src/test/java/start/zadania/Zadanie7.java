package start.zadania;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Wywołaj tylko te testy, które sprawią że komunikat będzie " zrobiłeś wszystko jak trzeba"
 * TODO: Nie usuwaj testów, nie zmieniaj kodu.
 * TODO: Możesz przestawić metody testowe.
 * TODO: Możesz zmienić ich nazwy.
 * TODO: jedyna dozwolona ingerencja w kod zmiana istniejącej adnotacji.
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Test(suiteName = "komunikat")
public class Zadanie7 {
    private String komunikat = "";

    @Test(suiteName = "komunikat", enabled = false)
    public void nie() {
        String dodatek = " nie";
        komunikat = dodatek;
        Assert.assertEquals(komunikat, dodatek);
    }

    @Test(suiteName = "komunikat", enabled = false)
    public void tak() {
        String dodatek = " tak";
        komunikat += dodatek;
        Assert.assertEquals(komunikat, dodatek);
    }

    @Test(suiteName = "komunikat", enabled = true)
    public void jak() {
        String dodatek = " jak";
        komunikat += dodatek;
        Assert.assertEquals(komunikat, dodatek);
    }

    @Test(suiteName = "komunikat", enabled = true)
    public void b_wszystko() {
        String dodatek = " wszystko";
        komunikat += dodatek;
        Assert.assertEquals(komunikat, dodatek);
    }

    @Test(suiteName = "komunikat", enabled = false)
    public void niestety() {
        String dodatek = " niestety";
        komunikat = dodatek;
        Assert.assertEquals(komunikat, dodatek);
    }

    @Test(suiteName = "komunikat", enabled = false)
    public void jakby() {
        String dodatek = " jakby";
        komunikat = dodatek;
        Assert.assertEquals(komunikat, dodatek);
    }

    @Test(suiteName = "komunikat", enabled = false)
    public void brawo() {
        String dodatek = " brawo";
        komunikat += dodatek;
        Assert.assertEquals(komunikat, dodatek);
    }

    @Test(suiteName = "komunikat", enabled = true)
    public void a_zrobiłeś() {
        String dodatek = " zrobiłeś";
        komunikat += dodatek;
        Assert.assertEquals(komunikat, dodatek);
    }

    @Test(suiteName = "komunikat", enabled = true)
    public void trzeba() {
        String dodatek = " trzeba";
        komunikat += dodatek;
        Assert.assertEquals(komunikat, dodatek);
    }

    @Test(suiteName = "komunikat", enabled = true)
    public void zzz() {
        System.out.println(komunikat);
    }

}
