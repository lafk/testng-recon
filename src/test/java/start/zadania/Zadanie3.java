package start.zadania;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

/**
 * Testy nie przechodzą, bo brakuje kawałków kodu Jawy. Dodaj te kawałki!
 *
 * TODO: nie zmieniając istniejącego kodu (a zwłaszcza asercji)
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Test
public class Zadanie3 {

    public void odpowiedziąJestTuzin() {

        int odp = 0;

        while(odp < 12) {
            odp += 2;
        }

        //zamiast powyższego - pętla while

        assertEquals(odp, 12);
    }

    public void odpowiedziąJest45() {

        int odp = 0;

        for (int i = 0; i < 10; i = i + 1) {
            // wpiszmy tu takie coś, co da nam sumę wartości i
            // w końcu 0+1+2+3+4+5+6+7+8+9 == 45
            // w końcu 1+1+1+1+1+1+1+1+1+1 == 10 - dlatego nie +1
            odp += i;
        }
        assertEquals(odp, 45);
    }

    public void odpowiedziąJestNapis() {
        StringBuilder napis = new StringBuilder();

        // w pętli for wygenerujmy oczekiwaną wartość tekstu dla poniższej asercji
        for (int i = 10; i > 0; i--) {
            napis.append(i);
        }

        assertEquals(napis.toString(), "10987654321");
    }
}
