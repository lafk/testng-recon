package start.zadania;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * TODO: Powinniśmy tu mieć 7 testów. Popraw co trzeba, by tyle było.
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Test
public class Zadanie5 {

    public void pierwszy() {
        short actual = (short)5;
        short expected = actual;
        Assert.assertEquals(actual, expected, "zmienne nie są równe");
    }

    public void drugi() {
        Assert.assertTrue(true, "problem z prawdą");
    }

    public void trzeci() {
    }

    @Test
    void czwarty() {

    }

    @Test
    private void piąty() {

    }

    @Test
    private void szósty() {
    }

    @Test(enabled = true)
    public void siódmy() {
        Assert.assertTrue(true, "położony test na porównaniu prawdy z prawdą?!");
        return;
    }

}
