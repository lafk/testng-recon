package start.zadania;

import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

/**
 * Poprawmy "zły test" poniżej, skopiowany z wcześniejszych materiałów!
 * TODO: Jedyne, co może świecić to nazwa testu (a nawet to bym zmienił)...
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Test
public class Zadanie4 {

    public void dobryTest() {
        assertEquals(1, 1);

        assertTrue(true); // true
        assertEquals(1, 1, "1 != 1 od kiedy?!"); // equals

        // co ciekawe, tu IDE nie widzi
        assertNotEquals(true, false);
        // co ciekawe, tu już owszem
        assertNotEquals(false, true); // notEquals

        assertTrue(true); // fail
    }

}
