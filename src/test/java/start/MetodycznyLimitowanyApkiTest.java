package start;

import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

/**
 * Testy jednostkowe z adnotacją na metodach.
 *
 * TODO: są trójkąty! Czyli działa! Ale... czy na pewno? Odpalmy i zobaczmy.
 * TODO: znajdź limity adnotacji @Test na metodzie drogą empiryczną (eksperymentuj!)
 *
 * @author LAFK_pl, Tomasz.Borek@gmail.com
 */
public class MetodycznyLimitowanyApkiTest {

    @Test
    public void testA() {
        String oczekiwany = "TestNG";
        String aktualny = "TestNG";
        assertEquals(aktualny, oczekiwany, "Porządek asercji to aktualny : oczekiwany, nie na odwrót (jak w JUnicie)");
    }

    @Test
    public void testB() {
        Assert.assertFalse(false);
    }


    @Test
    public void testC() {
        Assert.fail("położony celowo: void publiczny");
    }

    @Test
    void kolejnyTestCoZostałCelowoPołożony() {
        Assert.fail("położony celowo: void pakietowy");
    }

    @Test
    public boolean dlaczegoGoNieWidać() {
        Assert.fail("publiczny boolean");
        return true;
    }

    @Test
    boolean czyTenWidać() {
        Assert.fail("pakietowy boolean");
        return true;
    }

    class NotImplementedYetException extends Exception {
        public NotImplementedYetException(String msg) {
            super(msg);
        }
    }
    @Test
    private void zignorowanyCzyNieZnaleziony() throws Exception {
        throw new Exception("pusty test");
    }

    @Test
    private String niedostrzeżony() {
        Assert.fail("prywatny zwracający String");
        return "może tutaj?";
    }

    @Test
    public void tymRazemNaPewnoPrzejdzie() {
        Assert.assertTrue(5<7, "położony test na porównaniu prawdy z prawdą?!");
        return;
    }

}
