package start;

import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

/**
 * Testy jednostkowe z adnotacją na metodach.
 *
 *  @apiNote Test na klasie daje elastyczność, ale <ol>
 *     <li>jak go zapomnisz to bieda - metoda przeoczona</li>
 *     <li><code>public void</code> wciąż potrzebne - ma to sens</li>
 *     <li><code>Assert.fail</code> kładzie test</li>
 *  </ol>
 *
 * @author LAFK_pl, Tomasz.Borek@gmail.com
 */
public class MetodycznyApkiTest {

    @Test
    public void testA() {
        String oczekiwany = "TestNG";
        String aktualny = "TestNG";
        assertEquals(aktualny, oczekiwany, "Porządek asercji to aktualny : oczekiwany, nie na odwrót (jak w JUnicie)");
    }

    @Test
    public void testB() {
        Assert.assertFalse(false);
    }


    @Test
    public void testC() {
        Assert.fail("celowo położony test");
    }

    @Test
    void kolejnyTestCoZostałCelowoPołożony() {
        Assert.fail("celowo położony test");
    }

    @Test
    public boolean dlaczegoGoNieWidać() {
        Assert.fail("celowo położony test");
        return true;
    }

    @Test
    boolean czyTenWidać() {
        Assert.fail("celowo położony test");
        return true;
    }

    @Test
    private void zignorowanyCzyNieZnaleziony() {
        Assert.fail("celowo położony test");
    }

    @Test
    private String niedostrzeżony() {
        Assert.fail("celowo położony test");
        return "może tutaj?";
    }

    @Test
    public void tymRazemNaPewnoPrzejdzie() {
        Assert.assertTrue(true, "położony test na porównaniu prawdy z prawdą?!");
        return;
    }

}
