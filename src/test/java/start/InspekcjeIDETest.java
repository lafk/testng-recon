package start;

import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

/**
 * Ukazuje, jak inspekcje Intellij (Ultimate?) mogą nas uczyć. <br>
 * I jakie są dobre praktyki w pisaniu asercji.
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Test
public class InspekcjeIDETest {

    public void statyczneImportyRulez() {
        assertEquals(1, 1);

        assertEquals(1==1, true); // true
        assertTrue(1 == 1, "1 != 1 od kiedy?!"); // equals

        // co ciekawe, tu IDE nie widzi
        assertNotEquals(1==1, false);
        // co ciekawe, tu już owszem
        assertFalse(true == false); // notEquals

        assertFalse(true); // fail
    }

}
