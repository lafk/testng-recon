package start;

import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;

/**
 * Testy jednostkowe z adnotacją klasową:
 * Zauważ:<br>
 * - gdzie jest adnotacja <code>@Test</code><br>
 * - porządek asercji <code>assertEquals</code><br>
 *
 * @apiNote <ol>
 *     <li>Adnotacja na klasie? Znaczy, że na metodzie nie trzeba.</li>
 *     <li>JUnit odwraca naturalny porządek, mówiąc o oczekiwaniach najpierw a o realiach potem. TestNG mówi co mamy, a potem co byśmy chcieli mieć.</li>
 * </ol>
 *
 * TODO: jaką refaktoryzację należałoby zrobić na obu metodach testowych?
 * TODO: co jest nie tak z asercją w teście A?
 *
 * @author LAFK_pl, Tomasz.Borek@gmail.com
 */
@Test
public class KlasowyApkiTest {

    public void testA() {
        String oczekiwany = "TestNG";
        String aktualny = "TestNG";
        // DOKŁADNIE! Porządek asercji jest bardziej naturalny: co jest, co miało być
        assertEquals(aktualny, oczekiwany, "Porządek asercji to aktualny : oczekiwany, nie na odwrót (jak w JUnicie)");
    }

    public void testB() {
        assertFalse(false);
    }
}
