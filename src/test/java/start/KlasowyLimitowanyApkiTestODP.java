package start;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Testy jednostkowe z adnotacją klasową.
 *
 * Adnotacja na klasie? Znaczy, że na metodzie nie trzeba.

 * @apiNote Nie trzeba... jak długo metoda jest <code>public void</code>
 *
 * TODO: czy adnotacja @Test na metodzie gdzieś by pomogła?
 * TODO: napraw te testy!
 *
 * @author LAFK_pl, Tomasz.Borek@gmail.com
 */
@Test
public class KlasowyLimitowanyApkiTestODP {

    public void testA() {
        String oczekiwany = "TestNG";
        String aktualny = "TestNG";
        // DOKŁADNIE! Porządek asercji jest bardziej naturalny: co jest, co miało być
        Assert.assertEquals(aktualny, oczekiwany, "Porządek asercji to aktualny : oczekiwany, nie na odwrót (jak w JUnicie)");
    }

    public void testB() {
        Assert.assertFalse(false);
    }


    public void testC() {
        Assert.fail("celowo położony test");
    }

    @Test
    private void kolejnyTestCoZostałCelowoPołożony() {
        Assert.fail("celowo położony test");
    }

    @Test
    public void dlaczegoGoNieWidać() {
        Assert.fail("celowo położony test");
    }

    boolean czyTenWidać() {
        Assert.fail("celowo położony test");
        return true;
    }

    private void zignorowanyCzyNieZnaleziony() {
        Assert.fail("celowo położony test");
    }

    private String niedostrzeżony() {
        Assert.fail("celowo położony test");
        return "może tutaj?";
    }

    public void tymRazemNaPewnoPrzejdzie() {
        Assert.assertTrue(true, "położony test na porównaniu prawdy z prawdą?!");
        return;
    }

}
