package start;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Test;

/**
 * Ukazuje różne rezultaty testów różnej maści, biorąc pod uwagę specyfikatory dostępu, typy zwracane i adnotowanie klasy i metody. A nawet ignorowanie i błędy kompilacji!
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Test
public class TestRezultatów {

    @Test
    public void przechodzi_sukces() {
        Assert.assertEquals(1, 1);
    }
    @Test
    public void padaNaAsercji_porażka() {
        Assert.assertEquals(1, 2);
    }
    @Test
    public void błądWTeście_porażka() {
        // dzielenie przez zero manifestuje się po uruchomieniu
        Assert.assertTrue(2/0 == 1);
    }
    @Test(enabled = false)
    public void wyłączony_nie_znaczy_ignorowany() {
        Assert.assertEquals(1, 1);
    }
    @Test
    public void pominięty_znaczy_ignorowany() {
        throw new SkipException("ignorujemy mimo adnotacji na metodzie");
    }


    public void przechodziBoKlasaZaadnotowana() {
        Assert.assertEquals(1, 1);
    }
    //odtąd KZ == Klasa Zaadnotowana
    public void padaNaAsercji_KZ_porażka() {
        Assert.assertEquals(1, 2);
    }
    public void błądWTeście_KZ_porażka() {
        // dzielenie przez zero manifestuje się po uruchomieniu
        Assert.assertTrue(2/0 == 1);
    }

    void KZ_niewidoczny_znaczy_ignorowany() {
        Assert.assertEquals(1, 1);
    }

    public void KZ_pominięty() {
        throw new SkipException("ignorujemy mimo adnotacji na klasie");
    }


    public void przechodziMimoReturnBoVoidKZ_sukces() {
        Assert.assertEquals(1, 1);
        return;
    }
    public String trójkątKłamieKZ_ignorowanyBoNieVoidAString() {
        Assert.assertEquals(1, 1);
        return "String jest szczególny";
    }
    public Integer trójkątKłamieKZ_ignorowanyBoNieVoidAInteger() {
        Assert.assertEquals(1, 1);
        return Integer.valueOf(1);
    }
    public void podchwytliwe() {
        throw new AssertionError("to jest rzucane przez asercje");
    }
    public void podchwytliwe2() throws Throwable {
        throw new Throwable("rzucamy cokolwiek co można rzucić");
    }
}
